import json
import logging,sys, os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname( __file__ ), 'my_lib')))
from data_access import ComponentDao, RoleDao, TagDao
from database import db
from flask import Blueprint, jsonify
from schemas import ComponentSchema, RoleSchema, TagSchema

bp = Blueprint('api',__name__,url_prefix='/api')

@bp.route('/')
def home():

	return jsonify(Hello='You')

@bp.route('/components')
def components_list():

	db_models = ComponentDao(db.session).get_all()

	db_schema = ComponentSchema()

	return jsonify(data=db_schema.dump(db_models, many=True))

@bp.route('/tags')
def tags_list():

	db_models = TagDao(db.session).get_all()

	db_schema = TagSchema()

	return jsonify(data=db_schema.dump(db_models, many=True))

@bp.route('/roles')
def roles_list():

	db_models = RoleDao(db.session).get_all()

	db_schema = RoleSchema()

	return jsonify(data=db_schema.dump(db_models, many=True))