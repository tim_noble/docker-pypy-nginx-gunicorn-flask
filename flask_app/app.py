import logging, sys, os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname( __file__ ), 'my_lib')))
from blueprint_api import bp as api_bp
from blueprint_app import bp as app_bp
from database import db, SqlHelper
from flask import Flask
from os import getenv as os_getenv
import pypyodbc

def create_app():

	app = Flask(__name__)

	# init the db on the app
	app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
	app.config['SQLALCHEMY_ECHO'] = False
	app.config['SQLALCHEMY_DATABASE_URI'] = SqlHelper.build_conn_str()
	app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {'pool_recycle':int(os_getenv('SQL_ALCHEMY_POOL_RECYCLE_SECONDS')), 'pool_pre_ping': True, 'module':pypyodbc}
	db.init_app(app)

	# register bps
	app.register_blueprint(app_bp)
	app.register_blueprint(api_bp)

	return app