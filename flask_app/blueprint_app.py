import os, pdfkit, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname( __file__ ), 'my_lib')))
from flask import Blueprint, render_template, send_from_directory
from database import db
from data_access import ComponentDao
from service import PdfService
from uuid import uuid4

bp = Blueprint('default',__name__)

@bp.route('/')
def home():

	return render_template('index.html')

@bp.route('/pdf')
def pdf():

	models = ComponentDao(db.session).get_all()

	my_string = render_template('pdf.html', models=models)

	destination_dir, destination_fname = PdfService().from_string(my_string)

	return send_from_directory(destination_dir, destination_fname)