import logging, sys, os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname( __file__ ), 'my_lib')))
from os import environ as os_environ
from contextlib import contextmanager
from flask_sqlalchemy import SQLAlchemy

# new Flask SQLAlchemy
db = SQLAlchemy()

class SqlHelper:

	@staticmethod
	def build_conn_str():

		return '{prefix}://{user}:{password}@{host}:{port}/{db}?driver={driver}&Trusted_connection={trusted_connection}{additional_options}'.format(
			prefix='mssql+pyodbc',
			user=os_environ['DB_SQL_USER'],
			password=os_environ['DB_SQL_PASSWORD'],
			host=os_environ['DB_SQL_HOST'],
			port=os_environ['DB_SQL_PORT'],
			db=os_environ['DB_SQL_DB'],
			driver=os_environ['DB_SQL_DRIVER'],
			trusted_connection=os_environ['DB_SQL_TRUSTED_CONNECTION'],
			additional_options=os_environ['DB_SQL_ADDITIONAL_OPTIONS']
		)