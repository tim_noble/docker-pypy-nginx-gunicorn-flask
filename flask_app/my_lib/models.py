from database import db

schema_name='vc'

class Role(db.Model):

	__tablename__='role'
	__table_args__={'schema':schema_name}

	id = db.Column(db.String(50), primary_key=True)
	name = db.Column(db.String(50))

class Tag(db.Model):

	__tablename__ = "tag"
	__table_args__={'schema':schema_name}

	id = db.Column(db.String(50), primary_key=True)
	name = db.Column(db.String(50))
	components = db.relationship('Component', backref="tag")

class Component(db.Model):

	__tablename__='component'
	__table_args__={'schema':schema_name}

	id = db.Column(db.String(50), primary_key=True)
	name = db.Column(db.String(50))
	tag_id = db.Column(db.String(50), db.ForeignKey(f'{schema_name}.tag.id'))
