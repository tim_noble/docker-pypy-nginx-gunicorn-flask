from models import Component, Role, Tag

class Dao:

	def __init__(self, db_session):

		self.db_session = db_session

	def get_all(self):

		return self.db_session.query(self.MODEL).all()

class RoleDao(Dao):

	MODEL = Role

class ComponentDao(Dao):

	MODEL = Component

class TagDao(Dao):

	MODEL = Tag