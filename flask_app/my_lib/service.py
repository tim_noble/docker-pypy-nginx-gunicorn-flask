import pdfkit
from os import path as os_path, getenv as os_getenv
from uuid import uuid4

class PdfService:

	def __init__(self):

		path_wkhtmltopdf = os_getenv('PDF_KIT_PATH')

		if path_wkhtmltopdf != '':
			self.config = pdfkit.configuration(wkhtmltopdf=path_wkhtmltopdf)
		else:
			self.config = None

		self.destination_dir = os_path.join(os_path.dirname(os_path.dirname(__file__)),'my_files')
		self.destination_fname = '{}.pdf'.format(str(uuid4()))
		self.destination_path = os_path.join(self.destination_dir,self.destination_fname)

	def from_string(self, my_string):

		pdfkit.from_string(my_string, self.destination_path, configuration=self.config, options={'quiet':''})

		return self.destination_dir, self.destination_fname