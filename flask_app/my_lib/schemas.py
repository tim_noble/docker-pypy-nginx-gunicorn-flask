from marshmallow import Schema, fields

class RoleSchema(Schema):
	id = fields.Str()
	name = fields.Str()

class ComponentSchema(Schema):
	id = fields.Str()
	name = fields.Str()
	tag = fields.Nested('TagSchema', only=('id','name'))

class TagSchema(Schema):
	id = fields.Str()
	name = fields.Str()
	components = fields.Nested(ComponentSchema, many=True, only=('id','name'))