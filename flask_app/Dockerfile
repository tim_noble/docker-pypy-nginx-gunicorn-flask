# For more information, please refer to https://aka.ms/vscode-docker-python
ARG PARENT_IMAGE
FROM $PARENT_IMAGE:3.8-slim-buster

EXPOSE 5000

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Use the work dir
WORKDIR /usr/src/flask_app

# Install pip requirements
COPY requirements.txt .

RUN apt-get update && \
	apt-get install -y \
		build-essential \
		make \
		gcc \
		xfonts-75dpi \
		xfonts-base \
		fontconfig \ 
		libjpeg62-turbo \ 
		libx11-6 \ 
		libxcb1 \ 
		libxext6 \
		libxrender1 \
		curl \
		wget && \
	pip install --no-cache-dir -r requirements.txt && \
	curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
	curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list && \
	apt-get update && \
	ACCEPT_EULA=Y apt-get install -y msodbcsql17 && \
	apt-get install -y  && \
	wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.buster_amd64.deb && \
	dpkg -i wkhtmltox_0.12.6-1.buster_amd64.deb && \
	apt-get remove -y --purge make gcc build-essential && \
	apt-get autoremove -y

COPY . /usr/src/flask_app

RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /usr/src/flask_app
USER appuser

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
#CMD ["gunicorn", "--bind", "0.0.0.0:5000", "appserver:wsgi_app", "--timeout", "600", "--worker-class", "gevent", "--workers", "3", "--worker-connections", "100"]